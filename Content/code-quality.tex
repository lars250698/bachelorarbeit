\chapter{Code-Qualität}
\section{Abgrenzung zu Software-Qualität}
Der Begriff der Software-Qualität beschreibt die \enquote{Gesamtheit der Merkmale und Merkmalswerte eines Softwareprodukts, die sich auf dessen Eignung beziehen, festgelegte oder vorausgesetzte Erfordernisse zu erfüllen} \cite{balzert_lehrbuch_2010}.
Die ISO 25010 beschreibt die folgenden Merkmale der Software-Qualität: \cite{isoiec_isoiec_2010}
\begin{itemize}
    \item Funktionalität
    \item Effizienz
    \item Kompatibilität
    \item Benutzbarkeit
    \item Zuverlässigkeit
    \item Sicherheit
    \item Wartbarkeit
    \item Portabilität
\end{itemize}
Software-Qualität ist demnach eine Eigenschaft der gesamten Software, wenn diese als Black-Box betrachtet wird.

Im Gegensatz zur Software-Qualität gibt es zur Code-Qualität allerdings keine einheitliche Definition in der Literatur.
Die Begriffe von Code- und Software-Qualität werden dabei häufig sogar als Synonyme verwendet.
Diese Arbeit beschäftigt sich mit der Qualität von Sourcecode.
Eine Software soll hierbei nicht aus einer Black-Box-Betrachtung evaluiert werden, sondern aus einer inneren Sicht, welche sich primär mit den Interna der Software beschäftigt.
Daher wird Code-Qualität in dieser Arbeit definiert als die Qualität einer Software aus einer inneren, Sourcecode-fokussierten Betrachtung.

\section{Ziele}
Die Ziele von Software- und Code-Qualität überschneiden sich stark, sind allerdings nicht identisch.
Die Qualität einer Software wird maßgeblich von deren Sourcecode bestimmt, da dieser die Umsetzung der Anforderungen an die Software darstellt.
Daher gibt es einige Ziele der Software-Qualität, welche nicht von der Software-Qualität abhängen.
So hängt etwa die Benutzbarkeit einer Software primär vom UX-Design der Software ab, nicht von deren Code.
Zwar wird auch die Schnittstelle zum Nutzer durch Sourcecode realisiert, die Qualität des Designs der Schnittstelle hängt allerdings nicht direkt mit der Qualität des Sourcecodes zusammen, welcher sie implementiert.
Im Gegensatz hierzu hängt etwa die Effizienz einer Software direkt von der Effizienz des unterliegenden Sourcecodes ab, sie ist daher ein Ziel von Code-Qualität.
Weiter ergeben sich für die Code-Qualität Ziele, welche nur indirekt Ziele der Software-Qualität darstellen.
So ist etwa die Verständlichkeit von Code kein direktes Ziel der Software-Qualität, da diese bei einer Betrachtung der Software als Black-Box unerheblich ist.
Allerdings ist die Verständlichkeit von Code einer der wichtigsten Faktoren von Wartbarkeit \cite{boehm_software_2001}\cite{aggarwal_integrated_2002}\cite{buse_learning_2010}\cite{rugaber_use_2000}\cite{deimel_uses_1985}.
Daher werden für diese Arbeit die folgenden Ziele für Code-Qualität definiert:

\subsection{Effizienz} \label{code-quality-efficiency}
Effizienz beschreibt das Verhältnis von Aufwand zu Nutzen. Etwas ist effizient, wenn ein geringer Aufwand einen hohen Nutzen mit sich bringt.
In der Programmierung beschreibt Effizienz häufig die Effizienz von Algorithmen, welche sich mittels der O-Notation ausdrücken lässt.
Für den Blick auf Code-Qualität ist es allerdings sinnvoll, den Begriff der Effizienz mit Bezug auf das Softwaredesign zu verwenden.
So beschreibt das Attribut der Effizienz in dieser Arbeit die Effizienz der Konstrukte der Software.
Erfüllt ein Konstrukt mit geringstmöglichem Aufwand bei der Programmierung eine größtmögliche Funktion, so gilt es im Sinne der Code-Qualität als effizient.
Die Effizienz in der Code-Qualität wirkt sich direkt auf die Effizienz der Software-Qualität aus, da die fertige Software die Summe ihrer Konstrukte ist.
Sind diese Konstrukte allesamt effizient gestaltet, so ist auch die fertige Software als Summe effizienter Komponenten in sich effizient.

\subsection{Komplexität} \label{code-quality-complexity}
Das Attribut der Komplexität beschreibt in dieser Arbeit die architekturelle Komplexität der Software.
Allgemein versteht man in der theoretischen Informatik unter der Komplexität den Ressourcenbedarf eines Algorithmus in Abhängigkeit von der Eingabe.
Für die architekturelle Komplexität ist insbesondere die Anzahl der linear unabhängigen Pfade im Programmablauf von Bedeutung, wie von der zyklomatischen Komplexität nach McCabe beschrieben \cite{mccabe_complexity_1976}.
Die Komplexität einer Codebasis nimmt Einfluss auf Effizienz, Zuverlässigkeit, Sicherheit und Wartbarkeit einer Software.

\subsection{Verständlichkeit} \label{code-quality-understandability}
Die Verständlichkeit einer Codebasis beschreibt, wie gut ein Leser den Code verstehen kann.
Sie lässt sich auch als psychologische Komplexität auffassen.
Die Verständlichkeit einer Codebasis ist ausschlaggebend für mehrere Attribute der Software-Qualität.
So führt eine schlecht verständliche Codebasis etwa dazu, dass Entwickler der Software durch falsches oder fehlendes Verständnis der Vorgänge im Code Fehler einbauen, welche sich negativ auf Zuverlässigkeit, Sicherheit, Funktionalität, Benutzbarkeit und Effizienz auswirken können.
Weiterhin ist eine schwer verständliche Codebasis schwerer zu Warten und schwerer auf andere Systeme zu übertragen, was zu Abstrichen bei Wartbarkeit und Portabilität führt.

\subsection{Wiederverwendbarkeit} \label{code-quality-reuseability}
Die Wiederverwendbarkeit bezieht sich in der Code-Qualität darauf, wie gut einzelne Teilkomponenten im Code abstrahiert sind, um das Wiederverwenden der Komponente innerhalb der Codebasis zu ermöglichen.
Diese Teilkomponenten sind meist Funktionen oder, in der objektorientierten Programmierung, Klassen.
Sind Komponenten so gestaltet, dass sie für verschiedene Zwecke wiederverwendet werden können, so nimmt dies einen positiven Einfluss auf Funktionalität, Effizienz und Sicherheit einer Software, da mehr Entwicklungsaufwand in die Optimierung und Überprüfung der Komponente investiert werden kann.
Auch die Wartbarkeit einer Software wird durch eine hohe Wiederverwendbarkeit positiv beeinflusst, da Eingriffe zur Wartung auf die wiederverwendeten Komponenten isoliert wird. So muss für eine Änderung in der Logik des Programms ein Eingriff nur in der abstrahierten Komponente stattfinden, statt mehrere spezialisierte Komponenten anpassen zu müssen.
Weiter nimmt die Wiederverwendbarkeit Einfluss auf die Portabilität der Software, da durch eine hohe Wiederverwendung von Komponenten die Größe der Software insgesamt sinkt.

\subsection{Testbarkeit} \label{code-quality-testability}
Die Testbarkeit einer Codebasis gibt an, wie gut sie automatisiertes Testing zulässt.
Testing ist ein zentraler Bestandteil der Softwareentwicklung, da Testing das Ziel hat, die Qualität der Software zu sichern.
Somit nimmt eine hohe Testbarkeit und die damit verbundene Möglichkeit, die Software flächendeckend testen zu können, direkten Einfluss auf alle Merkmale der Software-Qualität.
Auch ist die Testbarkeit einer Software stark mit den anderen Attributen der Code-Qualität verwandt, da etwa eine hohe Komplexität oder eine schlechte Verständlichkeit der Codebasis dazu führt, dass diese auch schlechter zu testen ist.