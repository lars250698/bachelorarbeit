\chapter{Diskussion} \label{diskussion}

Insgesamt sind die Ergebnisse der Entwicklung des Reifegradmodells für Code-Qualität als positiv zu bewerten.
Das Modell bildet in seiner Gesamtheit die Reife von Code-Qualität hinreichend ab und zeigt einen klaren Weg von einer niedrigen zu einer hohen Code-Qualität auf.
Allerdings ist das Modell nicht frei von Schwächen, wie in \ref{dev:test} aufgezeigt wurde.

Diese Schwächen können teilweise auf allgemeine Probleme in der Abbildung von Code-Qualität zurückgeführt werden.
Qualität von Code hat viele Aspekte, von welchen sich einige nur schwer objektiv messen lassen.
Große Probleme ergeben sich etwa schon bei der Lesbarkeit von Code.
Ein zentraler Aspekt, welcher in Stufe 1 betrachtet wird, ist die Benennung von Bezeichnern.
Hier ist beschrieben, dass diese stets so benannt werden müssen, dass sie das Konzept, welche durch das benannte Programmelement implementiert wird, reflektieren.
Dies ist zwar eine klar definierte Anforderung, die Interpretation ist allerdings sehr subjektiv.
So ist in der Praxis nicht immer eindeutig, welcher Name ein Konzept reflektiert oder nicht reflektiert.
Auch können zwei Namen ein Konzept zutreffend beschreiben.
Hier liegt ein großer Interpretationsspielraum vor, weshalb die Anforderung in der Praxis oft nicht eindeutig bejaht werden kann.
Ähnliches gilt für die Anforderung, dass eine Funktion immer lediglich genau eine Aufgabe zu erfüllen hat.
Die Definition einer Aufgabe kann in einigen Fällen ebenfalls Interpretationsspielraum offen lassen.
Durch die zusätzliche Anforderung, dass eine Funktion auch immer genau eine Abstraktionsebene aufweisen darf, ist dieser Interpretationsspielraum allerdings geringer als bei der Benennung von Bezeichnern.

Auf Stufe 2 ergeben sich weitere Probleme dieser Art.
So ist auch die Definition von Robert C. Martins \textit{\acl{srp}} ähnlich abhängig von der Interpretation des Anwenders, da auch hier die Definition eines \textit{Akteurs} nicht immer klar umrissen werden kann.
So ist auch hier Interpretationsspielraum vorhanden, auch wenn sich dieser eher auf Edge-Cases beschränkt und deutlich geringer ist als in Stufe 1.

Dennoch zeigt \ref{dev:test}, dass Stufen 1 und 2 jeweils linguistische und architekturelle Lesbarkeit gut abbilden.
Trotz des Interpretationsspielraumes spiegeln die vorhandenen Anforderungen ihre Stufe gut wider.
Weiter geben sie klare Regeln für gut lesbaren Code vor, welche sich trotz des vorhandenen Interpretationsspielraumes positiv auf die Lesbarkeit des Codes auswirken.
Grund hierfür ist, dass zwar ein gewisser Interpretationsspielraum vorhanden ist, dieser sich allerdings nicht so drastisch ausweiten lässt, dass der zu bewertende Code auch bei sehr schlechter Lesbarkeit die Anforderungen erfüllt.
Es ist also innerhalb der Anforderungen durchaus Raum für Optimierung, ein komplettes Versagen der Anforderungen durch eine stark ausgeweitete Interpretation ist allerdings sehr unwahrscheinlich.
Eine mögliche Verbesserung wäre hier das Umbenennen der beiden Stufen, da deren Benennung in Teilen zu Verwirrung bei den Anwendern geführt hat, wie das schriftliche Feedback aus \ref{dev:test} zeigt.
Denkbare Namen wären hierbei für Stufe 1 \enquote{Lesbarer Code} und für Stufe 2 \enquote{Lesbares Design}.

Auf Stufe 3 beschränken sich die Schwächen eher auf die Sinnhaftigkeit und den Umfang von Wiederverwendbarkeit.
So äußerten mehrere Teilnehmer der Umfrage zur Bewertung des Modells, dass sie sich nicht im Klaren darüber sind, ob sich die Wiederverwendbarkeit auf die Codebasis selbst beschränkt, oder projektübergreifend stattfinden soll.
Diese Schwäche ist allerdings weniger den Anforderungen selbst, als dem Assessment des Modells zuzuschreiben, da sich die Wiederverwendbarkeit auf die zu bewertende Codebasis beschränkt.

Stufe 4 des Modells weist die größten Schwächen auf.
Zunächst wurde hier bemängelt, dass aus dem Assessment nicht hervorgeht, worauf sich der Begriff der Optimierung bezieht.
Hier muss eindeutiger aufgezeigt werden, dass sich diese Stufe die Dimensionen der Komplexität und Testbarkeit bezieht.
Weiter wurde bemängelt, dass diese Stufe lediglich die zyklomatische Komplexität anführt, welche alleine kein hinreichendes Kriterium für hinsichtlich Komplexität und Testbarkeit optimierten Code darstellt.
Hier muss aus dem Assessment besser hervorgehen, dass diese Anforderung nicht alleine, sondern nur gemeinsam mit den Anforderungen der vorangehenden Stufen als hinreichendes Kriterium gelten kann.
Eine weitere Problematik auf dieser Stufe besteht darin, dass sich Testbarkeit von Code nur schwer durch spezifische Anforderungen widerspiegeln lässt, sondern stark von anderen Aspekten der Code-Qualität abhängt, wie etwa der Lesbarkeit und der Komplexität.
Unter diesen Gesichtspunkten ließe sich argumentieren, dass die Stufe als solche überflüssig ist.
Allerdings ist die Anforderung der zyklomatischen Komplexität eher als Absicherung zu betrachten, welche gegen Code gerichtet ist, welcher trotz Erfüllung der Regeln auf Stufen 1 und 2 eine zu hohe Komplexität aufweist.
Weißt eine Funktion im Code trotz Erfüllung aller Anforderungen der vorangehenden Stufen eine zyklomatische Komplexität größer 10 auf, so kann die Codebasis nicht als testbar und hinsichtlich der Komplexität optimiert gelten.

Hinsichtlich Stufen 3 und 4 wäre eine mögliche Verbesserung diese zu einer gemeinsamen Stufe zusammenzufassen, welche sich nicht im speziellen mit Wiederverwendbarkeit, Komplexität, Effizienz oder Testbarkeit beschäftigt, sondern diese Punkte in einer gemeinsamen Stufe subsummiert.
Dies wäre aus mehreren Gründen eine Verbesserung des Modells.
Einerseits ist durch die Anforderungen der vorangehenden Stufen eine niedrige Komplexität des Codes bereits zu einem gewissen Maße sichergestellt.
Durch eine Zusammenfassung der Stufen ließe sich die Absicherung gegen Edge-Cases weiterhin beibehalten, ohne hierfür eine eigene Stufe aufzuwenden.
Weiter behandeln die Anforderungen aus Stufe 3 nicht exklusiv Wiederverwendbarkeit, sondern wirken sich auch in erheblichem Maße auf Komplexität, Effizienz und Testbarkeit aus.
Durch eine Ausweitung der Thematik der Stufe würde diese deren Inhalt entsprechend besser abbilden.
Dies geht auch aus dem schriftlichen Feedback aus \ref{dev:test} hervor.
Dieses zeigt, dass die Problematik der Stufen weniger in deren Anforderungen, als viel eher in deren Thematik liegt.
Diese Schwäche könnte eine Zusammenfassung der Stufen unter Anpassung der Thematik beheben.
Eine mögliche Thematik für diese neue Stufe wäre \enquote{Gutes Design} bzw. \enquote{Well-designed}.

Ein weiteres Problem des Modells besteht darin, dass die Stufen zwar als solche voneinander getrennt sind, die Übergänge zwischen ihnen allerdings in vielen Fällen fließend sind.
Code-Qualität ist komplex und die einzelnen Aspekte von Qualität sind stark miteinander verkoppelt.
So hängt etwa die Verständlichkeit (und damit Lesbarkeit) von Code von dessen Komplexität ab, da Komplexität für Menschen schwer zu erfassen ist.
Testbarkeit hängt wie bereits erläutert von Komplexität und von Verständlichkeit ab, da eine höhere Komplexität mehr Testfälle zur Folge hat, und die Implementierung der Tests vom Verständnis über den zu testenden Code abhängt.
Weiter spielt auch die Wiederverwendbarkeit eine Rolle für die Testbarkeit.
Kann Code wiederverwendet werden, reduziert sich die Anzahl an Testfällen, da nur die wiederverwendete Implementation getestet werden muss, statt jeder einzelnen duplizierten Implementation.
Nicht nur sind die einzelnen Aspekte von Code-Qualität voneinander abhängig, auch die Maßnahmen zur Verbesserung der Qualität verbessern nur selten einen einzelnen Aspekt.
So wirkt sich etwa die Regel, dass jede Funktion nur eine Aufgabe erfüllen und eine Abstraktionsebene aufweisen darf positiv auf die Lesbarkeit aus, sie beugt allerdings auch eine hohe Komplexität vor und erleichtert das Testing.
Ähnliches gilt für das \acl{srp} auf Klassenebene.
Dies reflektiert sich auch darin, dass die Stufen progressiv weniger Anforderungen aufweisen, da der jeweils zu reflektierende Aspekt von Code-Qualität bereits zu einem großen Teil von Anforderungen vorheriger Stufen abgedeckt ist.

Diese Probleme bedeuten jedoch nicht, dass ein Reifegradmodell ungeeignet für die Darstellung von Code-Qualität ist.
Zwar ist der deskriptive Aspekt eines Reifegradmodells nur bedingt erfüllbar, der präskriptive Aspekt hingegen ist als größte Stärke eines Reifegradmodells für Code-Qualität gegenüber der herkömmlichen Messung mittels statischer Codeanalyse zu betrachten.
Durch den stufenweisen Aufbau und die formulierten Anforderungen wird ein klarer Weg für die Verbesserung einer Codebasis aufgezeigt, welche die einzelnen Anforderungen an die Code-Qualität priorisiert.
Dadurch kann das vorgeschlagene Reifegradmodell für Code-Qualität den Entwicklungsprozess hin zu einer höheren Code-Qualität effizienter gestalten.
In welchem Maße das vorgeschlagene Modell hier für eine höhere Effizienz sorgen kann, ist nicht Gegenstand dieser Arbeit und kann nicht abschließend geklärt werden.
Hier bedarf es weiterer Forschung, etwa könnten die vorgeschlagenen Testverfahren aus \ref{dev:test-moegliche-verfahren} weiteren Aufschluss geben.