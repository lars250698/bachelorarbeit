\chapter{Reifegradmodelle}
\section{Definition} \label{maturity-model-definition}
Zum Verständnis von Reifegradmodellen ist zunächst eine Definition des Begriffs der \enquote{Reife} nötig.
Eine angebrachte Definition von Reife findet sich zum englischen \enquote{Maturity}: \enquote{The state of being complete, perfect or ready}\cite{maturity}.
Diese Definition wird auch von Mettler in \enquote{Maturity assessment models: a design science research approach} herangezogen \cite{mettler_maturity_2011}.
\enquote{Reife} ist demnach ein erstrebenswerter Zustand, auf den hingearbeitet werden soll.

Reifegradmodelle bestehen meist aus mehreren Stufen, welche in ihrer Gesamtheit einen erstrebenswerten Weg von einem initialen zu einem reifen Zustand abbilden sollen \cite{becker_developing_2009} \cite{poppelbus_what_2011}.
Ihre Anwendung soll einerseits einen Ist-Zustand abbilden, andererseits auch einen Weg aufzeigen, diesen zu verbessern, indem es klare Prioritäten zur Verbesserung vorgibt. \cite{iversen_situated_1999}.
Sie repräsentieren die stufenweise Entwicklung anhand eines definierten wünschenswerten Verbesserungsprozesses \cite{gottschalk_maturity_2009} \cite{poppelbus_what_2011}.
Reifegradmodelle beziehen sich hierbei meist auf Prozesse, Objekte oder Fähigkeiten \cite{mettler_maturity_2011}.

\section{Aufgaben eines Reifegradmodells} \label{maturity-model-goals}
Im Wesentlichen erfüllen Reifegradmodelle drei verschiedene Zwecke.
Zunächst erfüllt ein Reifegradmodell einen \textit{deskriptiven} Zweck.
Es soll den Ist-Zustand der betrachteten Entität anhand seiner Stufen abbilden, um ein diagnostisches Bild gegenüber Stakeholdern zu bieten \cite{becker_developing_2009} \cite{poppelbus_what_2011}.
Weiter erfüllt es einen \textit{präskriptiven} Zweck.
Es soll Maßnahmen und Prioritäten vorgeben, um einen reiferen Zustand zu erreichen \cite{becker_developing_2009}.
Die Maßnahmen sind hierbei durch den stufenweisen Aufbau des Modells nach Prioritäten sortiert, wodurch ein klarer Weg von einem initialen zu einem reifen Zustand angegeben wird.
Dabei ist wichtig, dass die Maßnahmen zur Verbesserung spezifisch und detailliert sind \cite{maier_assessing_2012}.
Letztlich können Reifegradmodelle auch einen \textit{komparativen} Zweck erfüllen.
Ist die Anwendung eines spezifischen Reifegradmodells standardisiert und verbreitet, so lassen sich anhand dessen verschiedene Organisationen vergleichen \cite{de_bruin_understanding_2005} \cite{maier_assessing_2012}.
Ob ein Modell einen komparativen Zweck erfüllen kann, hängt hierbei jedoch von externen Faktoren, wie etwa dessen Verbreitung und einer unabhängigen Bewertung ab \cite{poppelbus_what_2011}.

\section{Design-Prinzipien von Reifegradmodellen} \label{maturity-model-design-principles}
Pöppelbuß und Röglinger schlagen in ihrer Arbeit \enquote{What makes a useful maturity model?} mehrere Design-Prinzipien für Reifegradmodelle vor \cite{poppelbus_what_2011}.
Diese Design-Prinzipien stellen sicher, dass ein Reifegradmodell so entworfen ist, dass es die Aufgaben, die ein Reifegradmodell erfüllen soll, auch erfüllen kann.
Die Autoren teilen hierbei die Design-Prinzipien in drei Gruppen: Grundlegende Design-Prinzipien, Design-Prinzipien für die deskriptive Funktion von Reifegradmodellen und Design-Prinzipien für die präskriptive Funktion von Reifegradmodellen.

\paragraph*{Grundlegende Design-Prinzipien}
\begin{itemize}
    \item Grundlegende Informationen wie Nutzen, Zielgruppe und die zu untersuchende Entität müssen dokumentiert sein
    \item Die zentralen Konstrukte von Reife und dem Reifungsprozess müssen definiert sein
    \item Die zentralen Konstrukte der Anwendungsdomäne müssen definiert sein
    \item Die Grundlegenden Informationen, die zentralen Konstrukte und ihr Zusammenhang müssen zielgruppenorientiert dokumentiert sein
\end{itemize}

\paragraph*{Design-Prinzipien für die deskriptive Funktion von Reifegradmodellen}
\begin{itemize}
    \item Jede Stufe des Reifegradmodells muss Bewertungskriterien beinhalten
    \item Die Methodik der Bewertung ist dokumentiert
\end{itemize}

\paragraph*{Design-Prinzipien für die präskriptive Funktion von Reifegradmodellen}
\begin{itemize}
    \item Maßnahmen zur Verbesserung müssen für jede Stufe des Modells vorhanden sein
    \item Ein Decision Calculus ist vorhanden, welcher Entscheidungsträgern bei der Entscheidung hilft, welche Maßnahmen zur Verbesserung implementiert werden sollen
    \item Eine Zielgruppenorientierte Methodik zur Entscheidungsfindung ist vorhanden
\end{itemize}

\section{Entwicklung eines Reifegradmodells} \label{maturity-model-developing}
De Bruin et al. schlagen in ihrer Arbeit \enquote{Understanding the Main Phases of Developing a Maturity Assessment Model} einen sechsstufigen Entwicklungsprozess für Reifegradmodelle vor \cite{de_bruin_understanding_2005}.

\paragraph*{Scope}
Im ersten Schritt wird zunächst das Scope, also der Umfang des Reifegradmodells festgelegt. Zunächst muss hier die Entscheidung vorgenommen werden, ob das zu entwickelnde Modell eher allgemeingültig oder domänenspezifisch anwendbar sein soll.
Anschließend werden die Stakeholder bestimmt, welche in der Entwicklung des Modells assistieren.
Diese können akademischer Natur sein, etwa wenn die Entwicklung des Modells maßgeblich mittels akademischer Literatur durchgeführt wird, oder Anwender, wenn in der Entwicklung Erfahrung aus der Praxis einbezogen werden.
Die Entscheidungen, die in diesem Schritt getroffen werden, sind in Tabelle \ref{tab:scope} aufgezeigt. \cite{de_bruin_understanding_2005}

\begin{table}[h!]
    \caption{Entscheidungen Schritt 1: Scope}
    \begin{tabularx}{\textwidth}{|c|c|c|c|c|}
        \hline
        \textbf{Criterion}                & \multicolumn{4}{c|}{\textbf{Characteristic}}                                                           \\ \hline
        \textbf{Focus of Model}           & \multicolumn{2}{c|}{Domain Specific}         & \multicolumn{2}{c|}{General}                            \\ \hline
        \textbf{Developement Stakeholders} & Academia                                     & Practitioners                & Government & Combination \\ \hline
    \end{tabularx}
    \label{tab:scope}
\end{table}

\paragraph*{Design}
Im zweiten Schritt, der Design-Phase, wird die Architektur des Reifegradmodells festgelegt.
Die Kernfragen, die in diesem Schritt im Hinblick auf die Anwendung des Modells beantwortet werden, sind die folgenden:
\begin{itemize}
    \item Warum soll das Modell angewendet werden?
    \item Wie soll das Modell in verschiedenen Organisationsstrukturen angewendet werden?
    \item Wer muss in der Anwendung des Modells involviert sein?
    \item Was soll durch die Anwendung des Modells erreicht werden?
\end{itemize}
Die Entscheidungen, welche in diesem Schritt getroffen werden, sind in Tabelle \ref{tab:design} aufgezeigt.

\begin{table}[h!]
    \caption{Entscheidungen Schritt 2: Design}
    \begin{tabularx}{\textwidth}{|X|l|l|l|}
        \hline
        \multicolumn{1}{|c|}{\textbf{Criterion}} & \multicolumn{3}{c|}{\textbf{Characteristic}}                                                   \\ \hline
        \multicolumn{1}{|c|}{\textbf{Audience}}  & \multicolumn{3}{c|}{\begin{tabular}{c|c}
                Internal               & External           \\
                Executives, Management & Auditors, Partners
            \end{tabular}}                                                 \\ \hline
        \textbf{Method of Application}           & Self Assessment                                & Third Party Assisted & Certified Practitioner \\ \hline
        \textbf{Driver of Application}           & Internal Requirement                           & External Requirement & Both                   \\ \hline
        \textbf{Respondents}                     & Management                                     & Staff                & Business Partners      \\ \hline
        \textbf{Application}                     &
        \begin{tabular}[c]{@{}l@{}}1 entity / \\ 1 region\end{tabular}                &
        \begin{tabular}[c]{@{}l@{}}Multiple entities / \\ single region\end{tabular}                &
        \begin{tabular}[c]{@{}l@{}}Multiple entities / \\ multiple regions\end{tabular}                                                                                                                 \\ \hline
    \end{tabularx}
    \label{tab:design}
\end{table}

Die erste Entscheidung, welche hierbei getroffen werden muss, ist das Publikum des Modells.
Hierbei handelt es sich um diejenige Entität, welche Interesse an der aus dem fertigen Modell hervorgehenden Bewertung hat.
Dieses Publikum kann interner Natur sein, etwa das Management, oder externer Natur, etwa externe Auditoren, Partner oder Kunden.
Diese könnten mittels des Reifegradmodells etwa die Effizienz eines Prozesses oder die Qualität eines Produktes bestimmen.

Als nächstes muss die Anwendungsmethode festgelegt werden.
Diese kann durch eine Eigenbewertung, durch Mithilfe eines Dritten oder durch eine zertifizierte Bewertungsstelle stattfinden.
Außerdem muss die Anwendungsmotivation bestimmt werden.
Die Anwendung kann hierbei durch interne oder externe Anforderungen motiviert sein.

Weiter müssen die Respondents identifiziert werden.
Hierbei handelt es sich um diejenige Entität, welche Einfluss auf die Reife der zu bewertenden Sache nimmt und diese ggf. verbessern kann.
Sollte die zu bewertende Entität etwa ein Arbeitsprozess sein, so wären die Respondents das Management und die Angestellten, welche mittels des Prozesses arbeiten.

Abschließend ist die Application des Modells zu bestimmen.
Hier ist von Relevanz, wie viele Entitäten durch das Modell bewertet werden sollen, und in wie vielen Bereichen dies geschehen soll.
Die simpelste Methode ist hierbei die Bewertung einer Entität innerhalb eines Bereiches.
Dies ist insbesondere von Relevanz, wenn durch das Modell ein Prozess bewertet werden soll.
Dieser kann zwar in nur einem Bereich bewertet werden, dies könnte den Prozess allerdings zu sehr simplifizieren und somit die Bewertung verzerren.
Eine detailliertere Bewertung wäre somit möglich, indem man den Prozess in verschiedenen Bereichen bewertet.
Auch die Bewertung mehrerer Entitäten in einem Modell ist möglich.
Dies ist allerdings von geringer Relevanz für diese Arbeit.

Eine wesentliche Herausforderung der Design-Phase ist die Balance zwischen Komplexität und Simplizität.
Ein zu simples Modell kann die komplexe Realität nicht adäquat abbilden, während ein zu komplexes Modell zu Schwierigkeiten in der Anwendung führen kann.
Vorhandene Modelle nutzen zur Abbildung der Realität ein mehrstufiges Modell.
Dieser Ansatz wurde im Wesentlichen durch das Capability Maturity Model geprägt \cite{paulk_capability_1993}.
Zur Entwicklung dieser Stufen gibt es zwei Herangehensweisen:
In der Top-Down-Herangehensweise werden zunächst die Definitionen der Stufen entwickelt und anschließend die Metriken, welche zur Erfüllung der Definitionen erfüllt werden müssen, festgelegt.
In der Bottom-Up-Herangehensweise gilt das Gegenteil: Es werden zunächst die Metriken und anschließend eine passende Definition festgelegt.

Die Bottom-Up-Herangehensweise eignet sich, wenn die Merkmale von Reife bereits bekannt sind und nur noch den korrespondierenden Stufen zugeordnet werden müssen, Top-Down hingegen ist dann geeignet, wenn die Merkmale von Reife weitestgehend unbekannt sind, und erst im Prozess der Entwicklung des Reifegradmodells festgelegt werden sollen. \cite{de_bruin_understanding_2005}

\paragraph*{Populate}
Im dritten Schritt der Entwicklung wird das Reifegradmodell mit Inhalt befüllt.
In diesem Schritt werden nach der im vorherigen Schritt bestimmten Herangehensweise entweder zunächst die Definitionen der einzelnen Stufen definiert und diese anschließend mit den jeweiligen Anforderungen befüllt, oder vice versa.
Hierfür werden verschiedene Methoden angewandt.
Die wichtigste Methode zur Befüllung des Reifegradmodells ist eine umfassende Literaturrecherche, um Anforderungen für die Reife und kritische Erfolgsfaktoren zu identifizieren.
Außerdem können verschiedene Arten von Befragungen verwendet werden, um wichtige Metriken innerhalb der Domäne, in welche das Modell angewandt werden soll, zu identifizieren.
Hierfür eignen sich beispielsweise die Delphi-Methode, die Nominal-Group-Technique, Fokusgruppen oder Fallstudien.

\paragraph*{Test}
Nachdem das Reifegradmodell mit Inhalt befüllt wurde, muss dieses getestet werden.
Hierbei müssen der Aufbau des Modells sowie die Instrumente zur Bewertung auf Gültigkeit, Verlässlichkeit und Übertragbarkeit geprüft werden.
Beim Testen den Aufbaus muss geprüft werden, wie gut die Stufen des Modells die Abstufung der Reife der zu bewertenden Entität darstellt.
Hierfür sind Fokusgruppen und Befragungen geeignet.
Weiter muss geprüft werden, wie umfassend die Domäne repräsentiert wurde.
Hierfür sind das Ausmaß der Literaturrecherche und die Größe der Anwendungsdomäne indikativ.
Beim Testen der Werkzeuge muss geprüft werden, ob diese zur Messung der vorgesehenen Werte geeignet und ob deren Ergebnisse reproduzierbar sind.
Hierfür sind Befragungen und Umfragen geeignete Werkzeuge.

\paragraph*{Deploy}
Nachdem das Modell entwickelt und getestet wurde, erfolgt dessen Deployment in zwei Stufen.
Zunächst wird das Modell dort ausgerollt, wo es entwickelt und getestet wurde.
Hierbei ist insbesondere die Allgemeingültigkeit des Modells zu betrachten.
In der zweiten Stufe des Deployments wird das Modell außerhalb der entwickelnden Entitäten ausgerollt.
Wurde das Modell zum Einsatz innerhalb einer spezifischen Domäne und nicht als allgemeingültiges Modell entworfen, so können diese Entitäten etwa Firmen sein, welche ähnliche Grundvoraussetzungen aufweisen.
Durch das Übertragen des Modells auf Entitäten, welche nicht in den Entwicklungsprozess involviert waren, wird die Allgemeingültigkeit weiter verbessert.

\paragraph*{Maintain}
Nach dem Ausrollen des Modells muss dieses kontinuierlich gewartet werden.
Hierbei sind insbesondere die evolutionären Aspekte des Modells zu beachten.
So können etwa Konzessionen gemacht werden, um die Allgemeingültigkeit des Modells weiter zu steigern, wenn eine hohe Verbreitung des Modells dies erfordert.
Weiter müssen Maßnahmen getroffen werden, damit Änderungen an dem Modell nachvollziehbar und transparent sind, etwa indem das Modell unter Sourcenkontrolle gestellt wird.
Außerdem können weitere Maßnahmen in diesem Schritt notwendig werden, etwa das Bereitstellen von Trainingsmaterial, sollte die Anwendung des Modells durch eine unabhängige Stelle erfolgen.



