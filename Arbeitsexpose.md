# Arbeitsexposé Lars Eppinger

## Forschungsfrage Neu (Volles Maturity Modell entwickeln)
Lässt sich die Qualität einer Codebasis in einem mehrstufigen Reifegradmodell abbilden? Anhand welcher Kriterien kann man diese Qualität bewerten?

## Gliederung Neu

+ Einleitung & Forschungsfrage
+ Theoretischer Teil
  + Code-Qualität
    + Hier wird in die Thematik der Code-Qualität eingeführt. Es wird erläutert, warum Code-Qualität wichtig ist, welche Auswirkungen Code-Qualität auf eine Software und deren Entwicklungsprozess hat und anhand welcher Merkmale sich Code-Qualität abbilden lässt.
  + Reifegradmodelle
    + Hier wird das Konzept von Reifegradmodellen und ihrem Zweck erläutert. Weiter werden hier bereits bestehende Reifegradmodelle beleuchtet.
  + Reifegradmodell für Code-Qualität
    + Hier wird ein neues Reifegradmodell mit einem Fokus auf Code-Qualität entwickelt. Es wird sich an den bestehenden Reifegradmodellen orientiert.
+ Methodik
  + Hier wird erläutert, welche Methodik zur Überprüfung des neu entwickelten Reifegradmodells angewendet wird. Bei der Methodik wird es sich um ein Experiment handeln. Bei dem Experiment soll eine bestehende Code-Basis, welche sich in der niedrigsten Stufe des entwickelten Modells befindet, so refaktoriert werden, dass sie sukzessive innerhalb des Reifegradmodells aufsteigt. Anschließend werden für jede Stufe verschiedene Qualitätsmetriken mittels einer statischen Codeanalyse erhoben.
+ Ergebnisse
  + Hier wird das beschriebene Experiment durchgeführt und die Ergebnisse beschrieben. Anschließend wird das Ergebis evaluiert um zu sehen, ob das entwickelte Reifegradmodell tatsächlich die Qualität der Code-Basis abbilden konnte.
+ Diskussion
  + Hier werden Folgen und Ursachen der Ergebnisse beschrieben
+ Fazit


## Forschungsfrage Alt (Bestehendes Maturity Modell erweitern)
Welche Stufen in einem Capability Maturity Model lassen sich sinnvoll durch Anforderungen an die Qualität des Sourcecodes ergänzen und welche Anforderungen an einen Sourcecode sind geeignet, um im Sourcecode eine für die korrespondierenden Stufen in einem Capability Maturity Model geeignete Code-Qualität sicherzustellen?

## Gliederung Alt

+ Einleitung & Forschungsfrage
+ Code Quality
  + Hier wird in die Thematik der Code-Qualität eingeführt. Insbesondere wird hier erläutert, warum Code-Qulität wichtig ist und in welchen Bereichen sie sich auf den Softwareentwicklungsprozess auswirkt.
+ Capability Maturity Model
  + Grundlagen & Stand der Technik
    + Hier wird in die Thematik eines CMMs eingeführt. Es wird erläutert, was ein CMM ist und was die einzelnen Stufen in einem CMM abbilden.
  + Analyse der Stufen
    + Nach der Einführung in CMMs wird für jede Stufe im Einzelnen erörtert, ob sich diese Stufe sinnvoll mit Anforderungen an Code-Qualität erweitern und konkretisieren lässt. Hierfür wird ein Blick auf verschiedene Maturity Models und verwandte Modelle (bspw. CMMI, Spice) geworfen. Dieser Part ist zum größten Teil auf Literatur basiert. Hierbei wird ebenfalls erörtert, was die Anforderungen auf den verschiedenen Stufen sicherstellen sollen (Was ist das Ziel der Qualität? Lesbarkeit/Testbarkeit/Wartbarkeit...).
+ Methodik
  + Hier wird erörtert, welche Methodik geeignet ist, um die verschiedenen Ziele von Code Quality zu evaluieren bzw. zu quantifizieren, bspw. "Wie lesbar ist der Code?". 
+ Entwicklung der Qualitätsanforderungen
  + Hier werden nun für die erörterten Stufen im CMM Qualitätsmaßstäbe erörtert, welche das ebenfalls im vorherigen Abschnitt erörterte Ziel erfüllt. Bspw.: Im vorherigen Schritt wurde erörtert, dass auf Stufe 2 (Managed) im CMM die Lesbarkeit des Codes sichergestellt sein muss. Um diese sicher zu stellen, werden in diesem Schritt Metriken erörtert, welche die Lesbarkeit verbessern, etwa die zyklomatische Komplexität. Außerdem wird ein Threshold für die Metrik erörtert, ab wann die Anforderung (Lesbarkeit) als erfüllt gilt.
  + Um die Effektivität der entwickelten Qualitätsmaßstäbe zu messen, werden diese nun an einer Codebasis getestet, welche diese Maßstäbe noch nicht erfüllt. Diese wird in diesem Schritt so refactort, dass sie die entwickelten Maßstäbe erfüllt und anschließend wird evaluiert, ob die Anforderung durch das Refactoring erfüllt wurde (bspw. Ist der Code nach dem Refactoring besser lesbar?). Diese Evaluation geschieht anhand der im Abschnitt "Methoden" erörterten Methoden zur Quantifikation.
+ Fazit