# Code Quality Maturity Model Assessment

## Stufe 1: Linguistisch lesbar
* [ ] Es wird ein einheitlicher Styleguide für jede verwendete Programmiersprache verwendet und eingehalten
* [ ] Keine Logik im Code ist durch zwei verschiedene Namen bezeichnet
* [ ] Keine zwei Logiken im Code werden durch den selben Namen bezeichnet
* [ ] Jeder Identifier eines Programmteils ist so benannt, dass der Name die durch den Programmteil implementierte Logik bezeichnet
* [ ] Jede Funktion erfüllt genau eine Aufgabe
* [ ] Jede Funktion weist genau eine Abstraktionsebene auf
* [ ] Es sind keine Kommentare vorhanden, mit Ausnahme der folgenden:
    + Kommentare, welche aus jurstischen oder aus Compliance-Gründen notwendig sind
    + Kommentare, welche Informationen beinhalten, welche nicht durch besseren Code mitgeteilt werden können
    + Kommentare, welche die Gründe einer Entscheidung erläutern, welche ohne den Kommentar nicht oder nur schwer nachvollziehbar wären
    + Kommentare, welche die Bedeutung eines Arguments oder Rückgabewerts näher erläutern, wenn der Code nicht verändert werden kann
    + Kommentare, welche andere Programmierer vor nicht absehbaren Konsequenzen warnen
    + TODO-Kommentare
    + Kommentare, welche die Bedeutung eines Code-Abschnitts unterstreichen, welcher ohne den Kommentar als unbedeutend angesehen werden könnte
    + Javadoc in **öffentlichen** APIs

## Stufe 2: Architekturell lesbar
* [ ] Jede Klasse ist genau einem Akteur gegenüber verantwortlich (Das Single Responsibility Principle wird eingehalten)
* [ ] Keine Klasse weißt eine LCOM von unter 80% auf
* [ ] Jedes Modul kommuniziert nur mit Objekten aus seiner unmittelbaren Umgebung (Das Law of Demeter wird eingehalten)
* [ ] Fehler werden per Exception angezeigt, nicht als Rückgabewert

## Stufe 3: Portabel
* [ ] Jedes Objekt einer Klasse ist durch ein Objekt einer der Unterklassen des Objekts ersetzbar, ohne dass weitere Änderungen am Code notwendig sind (Das Liskov Substitution Principle wird eingehalten)
* [ ] Jede Implementation eines Interfaces nutzt all dessen Methoden (Das Interface Segregation Principle wird eingehalten)
* [ ] Es sind keine Code-Duplikationen vorhanden

## Stufe 4: Optimiert
* [ ] Keine Funktion hat eine zyklomatische Komplexität, welche höher als 10 ist
    + Ausgenommen hierbei sind Funktionen, welche große *switch-case*-Ausdrücke beinhalten

