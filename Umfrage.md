# Umfrage CQMM

- Wie gut repräsentiert das Modell die Reife von Code-Qualität?
- Wie gut repräsentiert das Modell die Abstufungen zwischen einem unreifen (schlechten) und einem reifen (guten) Code?


- Wie gut repräsentiert Stufe 1 linguistisch lesbaren Code?
- Wie gut repräsentiert Stufe 2 architekturell lesbaren Code?
- Wie gut repräsentiert Stufe 3 portablen Code?
- Wie gut repräsentiert Stufe 4 optimierten Code?


- Wie einfach ist das Assessment zu verstehen?
- Wie schwer schätzen sie die Einführung des Modells in ihrem Projekt ein?


https://www.surveymonkey.de/r/CZQSLQB
